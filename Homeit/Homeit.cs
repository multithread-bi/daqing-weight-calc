﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Resources;
using System.Globalization;

namespace Homeit
{
    public partial class Homeit : Form
    {
        public bool hasError;

        private List<Control> labelControls;

        public Homeit()
        {
            InitializeComponent();
            this.labelControls = this.GetAllControls(this);
        }

        public void validateIntValue(object sender, string label = null)
        {
            Control lbl = new Label();
            TextBox txt = (TextBox)sender;
            if (label == null) {
                lbl = this.getControlByName(this.getCorrespondingLabel(txt));
            } else {
                lbl = this.getControlByName(label);
            }
            int tempValue;
            bool result = int.TryParse(txt.Text, out tempValue);
            if (result || txt.TextLength == 0) {
                // no error
            } else {
                this.hasError = true;
                MessageBox.Show("Value of " + lbl.Text + " must be an integer type.", "Type Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void validateIntOrFloatValue(object sender, string label = null)
        {
            Control lbl = new Label();
            TextBox txt = (TextBox)sender;
            if (label == null) {
                lbl = this.getControlByName(this.getCorrespondingLabel(txt));
            } else {
                lbl = this.getControlByName(label);
            }
            float floatVal;
            int intVal;
            bool resultInt = int.TryParse(txt.Text, out intVal);
            bool resultFloat = float.TryParse(txt.Text, out floatVal);
            if (resultInt || resultFloat || txt.TextLength == 0) {
                // no error
            } else {
                this.hasError = true;
                txt.Text = "";
                MessageBox.Show("Value of " + lbl.Text + " must be an integer or float type.", "Type Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string getCorrespondingLabel(Control frmControl)
        {
            string controlType = frmControl.GetType().ToString();
            switch (controlType)
            {
                case "System.Windows.Forms.TextBox":
                    controlType = "textBox";
                    break;
                default: 
                    controlType = "textBox";
                    break;
            }
            string result = frmControl.Name.Replace(controlType, "label");
            return result;
        }

        Control getControlByName(string Name)
        {
            foreach (Control c in this.labelControls)
            {
                if (c.Name == Name)
                {
                    return c;
                }
            }
            return null;
        }

        private List<Control> GetAllControls(Control container, List<Control> list)
        {
            foreach (Control c in container.Controls)
            {
                if (c is Label) list.Add(c);
                if (c.Controls.Count > 0)
                    list = GetAllControls(c, list);
            }

            return list;
        }

        private List<Control> GetAllControls(Control container)
        {
            return GetAllControls(container, new List<Control>());
        }

        ///////////////////////////////////////////////////////////////////////
        ///////////////////////////// Nozzle Tab //////////////////////////////
        ///////////////////////////////////////////////////////////////////////

        private void textBox1_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE5();
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE5();
        }
        private void textBox3_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE5();
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE5();
        }

        /**
         * @A5 textBox1
         * @B5 textBox2
         * @C5 textBox3
         * @D5 textBox4
         * @E5 textBox5
         */
        private void calculateE5()
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "")
            {
                // A5 cell of the excel
                textBox5.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateE5(
                            float.Parse(textBox1.Text),
                            float.Parse(textBox2.Text),
                            float.Parse(textBox3.Text),
                            float.Parse(textBox4.Text)
                        )
                    );
            }
        }

        private void textBox10_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE7();
        }

        private void textBox9_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE7();
        }

        private void textBox8_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE7();
        }

        private void textBox7_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE7();
        }

        /**
         * @A7 textBox10
         * @B7 textBox9
         * @C7 textBox8
         * @D7 textBox7
         * @E7 textBox6
         */
        private void calculateE7()
        {
            if (textBox10.Text != "" && textBox9.Text != "" && textBox8.Text != "" && textBox7.Text != "")
            {
                // A5 cell of the excel
                textBox6.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateE7(
                            float.Parse(textBox10.Text),
                            float.Parse(textBox9.Text),
                            float.Parse(textBox8.Text),
                            float.Parse(textBox7.Text)
                        )
                    );
            }
        }

        private void textBox15_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE9();
        }

        private void textBox14_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
        }

        private void textBox13_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE9();
        }

        private void textBox12_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateE9();
        }

        /**
         * @A9 textBox15
         * @C9 textBox13
         * @D9 textBox12
         * @E9 textBox11
         */
        private void calculateE9()
        {
            if (textBox15.Text != "" && textBox13.Text != "" && textBox12.Text != "")
            {
                // A5 cell of the excel
                textBox11.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateE9(
                            float.Parse(textBox15.Text),
                            float.Parse(textBox13.Text),
                            float.Parse(textBox12.Text)
                        )
                    );
            }
        }

        private void textBox16_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateJ13();
            this.calculateK13();
        }

        private void textBox17_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateJ13();
        }

        private void textBox18_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateK13();
        }

        private void textBox19_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateK13();
        }

        private void textBox20_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateK13();
        }

        private void textBox21_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateJ13();
        }

        private void textBox22_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateK13();
        }

        private void textBox23_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateJ13();
            this.calculateK13();
        }

        /**
         * @A13 textBox16
         * @B13 textBox17
         * @C13 textBox18
         * @D13 textBox19
         * @E13 textBox20
         * @F13 textBox21
         * @G13 textBox22
         * @H13 textBox23
         * @I13 textBox24
         * @J13 textBox25
         */
        private void calculateJ13()
        {
            if (textBox16.Text != "" && textBox17.Text != "" && textBox21.Text != "" && textBox23.Text != "")
            {
                textBox24.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateJ13(
                        float.Parse(textBox16.Text),
                        float.Parse(textBox17.Text),
                        float.Parse(textBox21.Text),
                        float.Parse(textBox23.Text)
                        )
                    );
            }
        }

        private void calculateK13()
        {
            if (textBox16.Text != "" && textBox18.Text!="" && textBox19.Text != "" && textBox20.Text != "" && textBox22.Text != "" && textBox24.Text != "")
            {
                textBox25.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateK13(
                        float.Parse(textBox16.Text),
                        float.Parse(textBox18.Text),
                        float.Parse(textBox19.Text),
                        float.Parse(textBox20.Text),
                        float.Parse(textBox22.Text),
                        float.Parse(textBox24.Text)
                        )
                    );
            }
        }

        private void textBox35_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH32();
            this.calculateK32();
        }

        private void textBox34_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH32();
            this.calculateI32();
            this.calculateK32();
        }

        private void textBox33_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateI32();
        }

        private void textBox32_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH32();
            this.calculateK32();
        }

        private void textBox31_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH32();
            this.calculateK32();
        }

        private void textBox30_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH32();
            this.calculateK32();
        }

        private void textBox29_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateI32();
        }

        private void textBox36_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateK32();
        }

        /**
         * @A32 textBox35
         * @B32 textBox34
         * @C32 textBox33
         * @D32 textBox32
         * @E32 textBox31
         * @F32 textBox30
         * @G32 textBox29
         * @H32 textBox28
         * @I32 textBox27
         * @J32 textBox36
         * @K32 textBox37
         */
        private void calculateH32()
        {
            if (textBox35.Text != "" && textBox34.Text != "" && textBox32.Text != "" && textBox31.Text != "" && textBox30.Text != "")
            {
                textBox28.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateH32(
                        float.Parse(textBox35.Text),
                        float.Parse(textBox34.Text),
                        float.Parse(textBox32.Text),
                        float.Parse(textBox31.Text),
                        float.Parse(textBox30.Text)
                        )
                    );
                this.calculateI32();
            }
        }

        /**
         * @H32 is calculated in previous method
         */
        private void calculateI32()
        {
            if (textBox34.Text != "" && textBox33.Text != "" && textBox29.Text != "" && textBox28.Text != "")
            {
                textBox27.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateI32(
                        float.Parse(textBox34.Text),
                        float.Parse(textBox33.Text),
                        float.Parse(textBox29.Text),
                        float.Parse(textBox28.Text)
                        )
                    );
            }
        }

        private void calculateK32()
        {
            if (textBox35.Text != "" && textBox34.Text != "" && textBox32.Text != "" && textBox31.Text != "" && textBox30.Text != "" && textBox36.Text!="")
            {
                textBox37.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateK32(
                        float.Parse(textBox35.Text),
                        float.Parse(textBox34.Text),
                        float.Parse(textBox32.Text),
                        float.Parse(textBox31.Text),
                        float.Parse(textBox30.Text),
                        float.Parse(textBox36.Text)
                        )
                    );
            }
        }

        private void textBox49_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateG52();
            this.calculateJ52();
        }

        private void textBox48_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateG52();
            this.calculateH52();
        }

        private void textBox47_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH52();
        }

        private void textBox46_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateG52();
            this.calculateJ52();
        }

        private void textBox45_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateG52();
        }

        private void textBox44_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH52();
            this.calculateJ52();
        }

        private void textBox41_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateJ52();
        }

        /**
         * @A52 textBox49
         * @B52 textBox48
         * @C52 textBox47
         * @D52 textBox46
         * @E52 textBox45
         * @F52 textBox44
         * @G52 textBox43
         * @H52 textBox42
         * @I52 textBox41
         * @J52 textBox39
         */

        private void calculateG52()
        {
            if (textBox49.Text != "" && textBox48.Text != "" && textBox46.Text != "" && textBox45.Text != "")
            {
                textBox43.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateG52(
                        float.Parse(textBox49.Text),
                        float.Parse(textBox48.Text),
                        float.Parse(textBox46.Text),
                        float.Parse(textBox45.Text)
                        )
                    );
                this.calculateH52();
            }
        }

        /**
         * @G52 is calculated in previous method
         */
        private void calculateH52()
        {
            if (textBox48.Text != "" && textBox47.Text != "" && textBox44.Text != "" && textBox43.Text != "")
            {
                textBox42.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateH52(
                        float.Parse(textBox48.Text),
                        float.Parse(textBox47.Text),
                        float.Parse(textBox44.Text),
                        float.Parse(textBox43.Text)
                        )
                    );
            }
        }

        /**
         * @G52 is calculated in previous method
         */
        private void calculateJ52()
        {
            if (textBox49.Text != "" && textBox46.Text != "" && textBox45.Text != "" && textBox41.Text != "")
            {
                textBox39.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateJ52(
                        float.Parse(textBox49.Text),
                        float.Parse(textBox46.Text),
                        float.Parse(textBox45.Text),
                        float.Parse(textBox41.Text)
                        )
                    );
            }
        }

        private void textBox59_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateL72();
        }

        private void textBox58_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateI72();
            this.calculateL72();
        }

        private void textBox57_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateI72();
        }

        private void textBox56_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateL72();
        }

        private void textBox55_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateL72();
        }

        private void textBox54_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateL72();
        }

        private void textBox53_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateI72();
        }

        private void textBox38_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateH72();
            this.calculateL72();
        }

        private void textBox60_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.calculateL72();
        }

        /**
         * @A72 textBox59
         * @B72 textBox58
         * @C72 textBox57
         * @D72 textBox56
         * @E72 textBox55
         * @F72 textBox54
         * @G72 textBox53
         * @H72 textBox52
         * @I72 textBox51
         * @J72 textBox38
         * @K72 textBox60
         * @L72 textBox61
         */
        private void calculateH72()
        {
            if (textBox59.Text != "" && textBox58.Text != "" && textBox56.Text != "" && textBox55.Text != "" && textBox54.Text != "" && textBox38.Text != "")
            {
                textBox52.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateH72(
                        float.Parse(textBox59.Text),
                        float.Parse(textBox58.Text),
                        float.Parse(textBox56.Text),
                        float.Parse(textBox55.Text),
                        float.Parse(textBox54.Text),
                        float.Parse(textBox38.Text)
                        )
                    );
                this.calculateI72();
            }
        }
        
        /**
         * @H72 is calculated in previous method
         */
        private void calculateI72()
        {
            if (textBox58.Text != "" && textBox57.Text != "" && textBox53.Text != "" && textBox52.Text != "")
            {
                textBox51.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateI72(
                        float.Parse(textBox58.Text),
                        float.Parse(textBox57.Text),
                        float.Parse(textBox53.Text),
                        float.Parse(textBox52.Text)
                        )
                    );
            }
        }

        private void calculateL72()
        {
            if (textBox59.Text != "" && textBox58.Text != "" && textBox56.Text != "" && textBox55.Text != "" && 
                textBox54.Text != "" && textBox38.Text != "" && textBox60.Text != ""
                ) 
            {
                textBox61.Text = Helpers.convertFloatToString(
                        NozzleCalculator.calculateL72(
                        float.Parse(textBox59.Text),
                        float.Parse(textBox58.Text),
                        float.Parse(textBox56.Text),
                        float.Parse(textBox55.Text),
                        float.Parse(textBox54.Text),
                        float.Parse(textBox38.Text),
                        float.Parse(textBox60.Text)
                        )
                    );
            }
        }

        ///////////////////////////////////////////////////////////////////////
        /////////////////////////// Fixed Tube Tab ////////////////////////////
        ///////////////////////////////////////////////////////////////////////

        private void textBox79_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateE17();
            this.FT_calculateH12();
            this.FT_calculateI12();
            this.FT_calculateH26();
        }

        private void textBox63_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC16();
            this.FT_calculateC17();
            this.FT_calculateH4();
            this.FT_calculateH5();
            this.FT_calculateH7();
            this.FT_calculateI7();
        }

        private void textBox64_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC17();
            this.FT_calculateH7();
            this.FT_calculateI7();
        }

        private void textBox65_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC19();
            this.FT_calculateE13();
            this.FT_calculateE19();
            this.FT_calculateH15();
            this.FT_calculateI15();
            this.FT_calculateH17();
            this.FT_calculateH18();
            this.FT_calculateI18();
            this.FT_calculateH20();
            this.FT_calculateI20();
            this.FT_calculateH27();
            this.FT_calculateI27();
        }

        private void textBox66_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC19();
            this.FT_calculateI17();
            this.FT_calculateH18();
            this.FT_calculateI18();
            this.FT_calculateH20();
            this.FT_calculateI20();
        }

        private void textBox67_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC19();
            this.FT_calculateE13();
            this.FT_calculateE19();
            this.FT_calculateH15();
            this.FT_calculateI15();
            this.FT_calculateH19();
            this.FT_calculateI19();
            this.FT_calculateH27();
            this.FT_calculateI27();
        }

        private void textBox68_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC19();
            this.FT_calculateE13();
            this.FT_calculateE16();
            this.FT_calculateE18();
            this.FT_calculateH18();
            this.FT_calculateI18();
        }

        private void textBox69_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateE13();
            this.FT_calculateE16();
            this.FT_calculateE18();
        }

        private void textBox70_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateE13();
            this.FT_calculateE16();
            this.FT_calculateE18();
            this.FT_calculateH15();
            this.FT_calculateI15();
        }

        private void textBox71_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateC16();
            this.FT_calculateH6();
            this.FT_calculateI6();
        }

        private void FT_calculateC16()
        {
            if (textBox63.Text != "" && textBox71.Text != "")
            {
                textBox72.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC16(
                        float.Parse(textBox63.Text),
                        float.Parse(textBox71.Text)
                        )
                    );
                this.FT_calculateC18();
                this.FT_calculateC20();
                this.FT_calculateH6();
                this.FT_calculateI6();
            }
        }

        private void FT_calculateC17()
        {
            if (textBox63.Text != "" && textBox64.Text != "")
            {
                textBox73.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC17(
                        float.Parse(textBox63.Text),
                        float.Parse(textBox64.Text)
                        )
                    );
                this.FT_calculateC18();
                this.FT_calculateC20();
            }
        }

        private void FT_calculateC18()
        {
            if (textBox72.Text != "" && textBox73.Text != "")
            {
                textBox74.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC18(
                        float.Parse(textBox72.Text),
                        float.Parse(textBox73.Text)
                        )
                    );
            }
        }

        private void FT_calculateC19()
        {
            if (textBox65.Text != "" && textBox66.Text != "" && textBox67.Text != "" && textBox68.Text != "")
            {
                textBox75.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC19(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox66.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox68.Text)
                        )
                    );
                this.FT_calculateC20();
            }
        }

        private void FT_calculateC20()
        {
            if (textBox72.Text != "" && textBox73.Text != "" && textBox75.Text != "")
            {
                textBox76.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC20(
                        float.Parse(textBox72.Text),
                        float.Parse(textBox73.Text),
                        float.Parse(textBox75.Text)
                        )
                    );
                this.FT_calculateE7();
            }
        }

        /**
         * @H6 textBox106
         * @H7 textBox108
         */
        private void FT_calculateC23()
        {
            if (textBox106.Text != "" && textBox108.Text != "")
            {
                textBox76.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC23(
                        float.Parse(textBox106.Text),
                        float.Parse(textBox108.Text)
                        )
                    );
                this.FT_calculateC28();
            }
        }

        /**
         * @H19 textBox124
         */
        private void FT_calculateC24()
        {
            if (textBox124.Text != "")
            {
                textBox62.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC24(
                        float.Parse(textBox124.Text)
                        )
                    );
                this.FT_calculateC28();
            }
        }

        /**
         * @H15 textBox116
         */
        private void FT_calculateC25()
        {
            if (textBox116.Text != "")
            {
                textBox80.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC25(
                        float.Parse(textBox116.Text)
                        )
                    );
                this.FT_calculateC28();
            }
        }

        /**
         * @H12 textBox112
         */
        private void FT_calculateC26()
        {
            if (textBox112.Text != "")
            {
                textBox81.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC26(
                        float.Parse(textBox112.Text)
                        )
                    );
                this.FT_calculateC28();
            }
        }

        /**
         * @H28 textBox131
         */
        private void FT_calculateC27()
        {
            if (textBox131.Text != "")
            {
                textBox82.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC27(
                        float.Parse(textBox131.Text)
                        )
                    );
                this.FT_calculateC28();
            }
        }

        /**
         * @C23 textBox76
         * @C24 textBox62
         * @C25 textBox80
         * @C26 textBox81
         * @C27 textBox82
         */
        private void FT_calculateC28()
        {
            if (textBox76.Text != "" && textBox62.Text != "" && textBox80.Text != "" && textBox81.Text != "" && textBox82.Text != "")
            {
                textBox83.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateC28(
                        float.Parse(textBox76.Text),
                        float.Parse(textBox62.Text),
                        float.Parse(textBox80.Text),
                        float.Parse(textBox81.Text),
                        float.Parse(textBox82.Text)
                        )
                    );
            }
        }

        /**
         * @E4 textBox89
         * @E7 textBox90
         * @E10 textBox91
         * @E13 textBox92
         * @E16 textBox84
         * @E17 textBox85
         * @E18 textBox86
         * @E19 textBox87
         * @E20 textBox88
         * @E23 textBox98
         * @E24 textBox97
         * @E25 textBox96
         * @E26 textBox95
         * @E27 textBox94
         * @E28 textBox93
         */

        private void FT_calculateE4()
        {
            if (textBox88.Text != "")
            {
                textBox89.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE4(
                        float.Parse(textBox88.Text)
                        )
                    );
                this.FT_calculateE10();
            }
        }

        private void FT_calculateE7()
        {
            if (textBox76.Text != "")
            {
                textBox90.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE7(
                        float.Parse(textBox76.Text)
                        )
                    );
                this.FT_calculateE10();
            }
        }

        private void FT_calculateE10()
        {
            if (textBox89.Text != "" && textBox90.Text != "")
            {
                textBox91.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE10(
                        float.Parse(textBox89.Text),
                        float.Parse(textBox90.Text)
                        )
                    );
            }
        }

        private void FT_calculateE13()
        {            
            if (textBox65.Text != "" && textBox67.Text != "" && textBox86.Text != "")
            {
                textBox92.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE13(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox86.Text)
                        )
                    );
            }
        }

        private void FT_calculateE16()
        {
            if (textBox68.Text != "" && textBox69.Text != "" && textBox70.Text != "")
            {
                textBox84.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE16(
                        float.Parse(textBox68.Text),
                        float.Parse(textBox69.Text),
                        float.Parse(textBox70.Text)
                        )
                    );
                this.FT_calculateE17();
                this.FT_calculateH10();
            }
        }

        /**
         * @E16 is calculated in previous method
         */
        private void FT_calculateE17()
        {
            if (textBox79.Text != "" && textBox84.Text != "")
            {
                textBox85.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE17(
                        float.Parse(textBox79.Text),
                        float.Parse(textBox84.Text)
                        )
                    );
                this.FT_calculateE20();
            }
        }

        private void FT_calculateE18()
        {
            if (textBox68.Text != "" && textBox69.Text != "" && textBox70.Text != "")
            {
                textBox86.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE18(
                        float.Parse(textBox68.Text),
                        float.Parse(textBox69.Text),
                        float.Parse(textBox70.Text)
                        )
                    );
                this.FT_calculateE19();
            }
        }

        /**
         * @E18 is calculated in previous method
         */
        private void FT_calculateE19()
        {
            if (textBox65.Text != "" && textBox67.Text != "" && textBox86.Text != "")
            {
                textBox87.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE19(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox86.Text)
                        )
                    );
                this.FT_calculateE20();
            }
        }

        /**
         * @E17 is calculated in previous method
         * @E19 is calculated in previous method
         */
        private void FT_calculateE20()
        {
            if (textBox85.Text != "" && textBox87.Text != "")
            {
                textBox88.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE20(
                        float.Parse(textBox85.Text),
                        float.Parse(textBox87.Text)
                        )
                    );
                this.FT_calculateE4();
            }
        }

        private void FT_calculateE23()
        {
            if (textBox105.Text != "" && textBox107.Text != "")
            {
                textBox98.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE23(
                        float.Parse(textBox105.Text),
                        float.Parse(textBox107.Text)
                        )
                    );
                this.FT_calculateE28();
            }
        }

        /**
         * @I19 is calculated in a method below
         */
        private void FT_calculateE24()
        {
            if (textBox123.Text != "")
            {
                textBox97.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE24(
                        float.Parse(textBox123.Text)
                        )
                    );
                this.FT_calculateE28();
            }
        }

        /**
         * @I15 is calculated in a method below
         */
        private void FT_calculateE25()
        {
            if (textBox115.Text != "")
            {
                textBox96.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE25(
                        float.Parse(textBox115.Text)
                        )
                    );
                this.FT_calculateE28();
            }
        }

        /**
         * @I19 is calculated in a method below
         */
        private void FT_calculateE26()
        {
            if (textBox115.Text != "")
            {
                textBox95.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE26(
                        float.Parse(textBox115.Text)
                        )
                    );
                this.FT_calculateE28();
            }
        }

        /**
         * @I28 is calculated in a method below
         */
        private void FT_calculateE27()
        {
            if (textBox130.Text != "")
            {
                textBox94.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE27(
                        float.Parse(textBox130.Text)
                        )
                    );
                this.FT_calculateE28();
            }
        }

        /**
         * @E23 is calculated in a previous method
         * @E24 is calculated in a previous method
         * @E25 is calculated in a previous method
         * @E26 is calculated in a previous method
         * @E27 is calculated in a previous method
         */
        private void FT_calculateE28()
        {
            if (textBox98.Text != "" && textBox97.Text != "" && textBox96.Text != "" && textBox95.Text != "" && textBox94.Text != "")
            {
                textBox93.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateE28(
                        float.Parse(textBox98.Text),
                        float.Parse(textBox97.Text),
                        float.Parse(textBox96.Text),
                        float.Parse(textBox95.Text),
                        float.Parse(textBox94.Text)
                        )
                    );
            }
        }

        private void textBox102_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateH6();
            this.FT_calculateI6();
            this.FT_calculateH7();
            this.FT_calculateI7();
        }

        private void textBox109_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateH12();
            this.FT_calculateI12();
        }

        private void textBox114_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateH15();
            this.FT_calculateI15();
        }

        private void textBox113_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
        }

        private void textBox114_Layout(object sender, LayoutEventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            // check with stakeholder
        }

        private void textBox125_Leave(object sender, EventArgs e)
        {
            this.validateIntValue(sender);
            this.FT_calculateH28();
            this.FT_calculateI28();
        }

        private void textBox126_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateH27();
            this.FT_calculateI27();
        }

        private void textBox128_Leave(object sender, EventArgs e)
        {
            this.validateIntOrFloatValue(sender);
            this.FT_calculateH26();
        }

        private void textBox127_Leave(object sender, EventArgs e)
        {
            this.validateIntValue(sender);
            this.FT_calculateH27();
            this.FT_calculateI27();
        }

        private void FT_calculateH4()
        {
            if (textBox63.Text != "")
            {
                textBox103.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH4(
                        float.Parse(textBox63.Text)
                        )
                    );
                this.FT_calculateH6();
                this.FT_calculateI6();
                this.FT_calculateH12();
                this.FT_calculateI12();
            }
        }

        private void FT_calculateH5()
        {
            if (textBox63.Text != "")
            {
                textBox104.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH5(
                        float.Parse(textBox63.Text)
                        )
                    );
                this.FT_calculateH6();
                this.FT_calculateI6();
            }
        }

        /**
         * @H2 is constant
         * @H3 is calculated in a previous method
         * @H4 is calculated in a previous method
         * @H5 is calculated in a previous method
         */
        private void FT_calculateH6()
        {
            if (textBox71.Text != "" && textBox72.Text != "" && textBox100.Text != "" && textBox102.Text != "" && textBox103.Text != "" && textBox104.Text != "")
            {
                textBox106.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH6(
                        float.Parse(textBox71.Text),
                        float.Parse(textBox72.Text),
                        float.Parse(textBox100.Text),
                        float.Parse(textBox102.Text),
                        float.Parse(textBox103.Text),
                        float.Parse(textBox104.Text)
                        )
                    );
                this.FT_calculateC23();
            }
        }

        private void FT_calculateI6()
        {
            if (textBox71.Text != "" && textBox72.Text != "" && textBox101.Text != "" && textBox102.Text != "" && textBox103.Text != "" && textBox104.Text != "")
            {
                textBox105.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI6(
                        float.Parse(textBox71.Text),
                        float.Parse(textBox72.Text),
                        float.Parse(textBox102.Text),
                        float.Parse(textBox103.Text),
                        float.Parse(textBox104.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateE23();
            }
        }

        private void FT_calculateH7()
        {
            if (textBox63.Text != "" && textBox64.Text != "" && textBox102.Text != "")
            {
                textBox105.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH7(
                        float.Parse(textBox63.Text),
                        float.Parse(textBox64.Text),
                        float.Parse(textBox100.Text),
                        float.Parse(textBox102.Text)
                        )
                    );
                this.FT_calculateC23();
            }
        }

        private void FT_calculateI7()
        {
            if (textBox63.Text != "" && textBox64.Text != "" && textBox102.Text != "")
            {
                textBox105.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI7(
                        float.Parse(textBox63.Text),
                        float.Parse(textBox64.Text),
                        float.Parse(textBox102.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateE23();
            }
        }

        private void FT_calculateH10()
        {
            if (textBox84.Text != "")
            {
                textBox110.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH10(
                        float.Parse(textBox84.Text)
                        )
                    );
            }
        }

        private void FT_calculateH12()
        {
            if (textBox79.Text != "" && textBox103.Text != "" && textBox109.Text != "")
            {
                textBox112.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH12(
                        float.Parse(textBox79.Text),
                        float.Parse(textBox100.Text),
                        float.Parse(textBox109.Text),
                        float.Parse(textBox103.Text)
                        )
                    );
                this.FT_calculateC26();
            }
        }

        private void FT_calculateI12()
        {
            if (textBox79.Text != "" && textBox103.Text != "" && textBox109.Text != "")
            {
                textBox111.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI12(
                        float.Parse(textBox79.Text),
                        float.Parse(textBox109.Text),
                        float.Parse(textBox103.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateE26();
            }
        }

        private void FT_calculateH15()
        {
            if (textBox65.Text != "" && textBox67.Text != "" && textBox70.Text != "" && textBox114.Text != "")
            {
                textBox116.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH15(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox70.Text),
                        float.Parse(textBox100.Text),
                        float.Parse(textBox114.Text)
                        )
                    );
                this.FT_calculateC25();
            }
        }

        private void FT_calculateI15()
        {
            if (textBox65.Text != "" && textBox67.Text != "" && textBox70.Text != "" && textBox114.Text != "")
            {
                textBox115.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI15(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox70.Text),
                        float.Parse(textBox114.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateE25();
            }
        }

        private void FT_calculateH17()
        {
            if (textBox65.Text != "")
            {
                textBox118.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH17(
                        float.Parse(textBox65.Text)
                        )
                    );
            }
        }

        private void FT_calculateI17()
        {
            if (textBox66.Text != "")
            {
                textBox117.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI17(
                        float.Parse(textBox66.Text)
                        )
                    );
            }
        }

        private void FT_calculateH18()
        {
            if (textBox65.Text != "" && textBox66.Text != "" && textBox68.Text != "")
            {
                textBox120.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH18(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox66.Text),
                        float.Parse(textBox68.Text),
                        float.Parse(textBox100.Text)
                        )
                    );
                this.FT_calculateH19();
            }
        }

        private void FT_calculateI18()
        {
            if (textBox65.Text != "" && textBox66.Text != "" && textBox68.Text != "")
            {
                textBox119.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI18(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox66.Text),
                        float.Parse(textBox68.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateI19();
            }
        }

        /**
         * @H18 is calculated in a previous method
         */
        private void FT_calculateH19()
        {
            if (textBox120.Text != "" && textBox67.Text != "")
            {
                textBox124.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH19(
                        float.Parse(textBox119.Text),
                        float.Parse(textBox67.Text)
                        )
                    );
                this.FT_calculateC24();
            }
        }

        /**
         * @I18 is calculated in a previous method
         */
        private void FT_calculateI19()
        {
            if (textBox119.Text != "" && textBox67.Text != "")
            {
                textBox123.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI19(
                        float.Parse(textBox119.Text),
                        float.Parse(textBox67.Text)
                        )
                    );
                this.FT_calculateE24();
            }
        }

        private void FT_calculateH20()
        {
            if (textBox65.Text != "" && textBox66.Text != "")
            {
                textBox122.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH20(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox66.Text),
                        float.Parse(textBox100.Text)
                        )
                    );
            }
        }

        private void FT_calculateI20()
        {
            if (textBox65.Text != "" && textBox66.Text != "")
            {
                textBox121.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI20(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox66.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
            }
        }

        private void FT_calculateH26()
        {
            if (textBox79.Text != "" && textBox128.Text != "")
            {
                textBox129.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH26(
                        float.Parse(textBox79.Text),
                        float.Parse(textBox128.Text)
                        )
                    );
                this.FT_calculateH27();
                this.FT_calculateI27();
            }
        }

        private void FT_calculateH27()
        {
            if (textBox65.Text != "" && textBox67.Text != "" && textBox126.Text != "" && textBox127.Text != "" && textBox129.Text != "")
            {
                textBox133.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH27(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox100.Text),
                        float.Parse(textBox126.Text),
                        float.Parse(textBox127.Text),
                        float.Parse(textBox129.Text)
                        )
                    );
                this.FT_calculateH28();
            }
        }

        private void FT_calculateI27()
        {
            if (textBox65.Text != "" && textBox67.Text != "" && textBox126.Text != "" && textBox127.Text != "" && textBox129.Text != "")
            {
                textBox132.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI27(
                        float.Parse(textBox65.Text),
                        float.Parse(textBox67.Text),
                        float.Parse(textBox126.Text),
                        float.Parse(textBox127.Text),
                        float.Parse(textBox129.Text),
                        float.Parse(textBox101.Text)
                        )
                    );
                this.FT_calculateI28();
            }
        }

        /**
         * @H27 is calculated in previous method
         */
        private void FT_calculateH28()
        {
            if (textBox125.Text != "" && textBox133.Text != "")
            {
                textBox131.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateH28(
                        float.Parse(textBox125.Text),
                        float.Parse(textBox133.Text)
                        )
                    );
                this.FT_calculateC27();
            }
        }

        /**
         * @I27 is calculated in previous method
         */
        private void FT_calculateI28()
        {
            if (textBox125.Text != "" && textBox132.Text != "")
            {
                textBox130.Text = Helpers.convertFloatToString(
                        FixedTubeCalculator.calculateI28(
                        float.Parse(textBox125.Text),
                        float.Parse(textBox132.Text)
                        )
                    );
                this.FT_calculateE27();
            }
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            englishToolStripMenuItem.Checked = true;
            中文ToolStripMenuItem.Checked = false;
            CultureInfo ci = new CultureInfo("en-US");
            ChangeLanguage(ci);
        }

        private void 中文ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            englishToolStripMenuItem.Checked = false;
            中文ToolStripMenuItem.Checked = true;
            CultureInfo ci = new CultureInfo("zh-CN");
            ChangeLanguage(ci);
        }

        private void Homeit_Load(object sender, EventArgs e)
        {
            if (englishToolStripMenuItem.Checked == true) {
                    CultureInfo ci = new CultureInfo("en-US");
                    ChangeLanguage(ci);
            } else {
                CultureInfo ci = new CultureInfo("zh-CN");
                ChangeLanguage(ci);
            }
        }

        private void ChangeLanguage(CultureInfo ci)
        {
            Assembly a = Assembly.Load("Homeit");
            ResourceManager rm = new ResourceManager("Homeit.Lang.langres", a);
            switchLanguageToolStripMenuItem.Text = rm.GetString("switchLanguageToolStripMenuItem", ci);
            groupBox1.Text = rm.GetString("groupBox1", ci);
            groupBox2.Text = rm.GetString("groupBox2", ci);
            groupBox3.Text = rm.GetString("groupBox3", ci);
            groupBox4.Text = rm.GetString("groupBox4", ci);
            groupBox5.Text = rm.GetString("groupBox5", ci);
            groupBox6.Text = rm.GetString("groupBox6", ci);
            Nozzle.Text = rm.GetString("Nozzle", ci);
            FixedTube.Text = rm.GetString("FixedTube", ci);
            label1.Text = rm.GetString("label1", ci);
            label2.Text = rm.GetString("label2", ci);
            label3.Text = rm.GetString("label3", ci);
            label4.Text = rm.GetString("label4", ci);
            label5.Text = rm.GetString("label5", ci);
            label6.Text = rm.GetString("label6", ci);
            label7.Text = rm.GetString("label7", ci);
            label8.Text = rm.GetString("label8", ci);
            label9.Text = rm.GetString("label9", ci);
            label10.Text = rm.GetString("label10", ci);
            label11.Text = rm.GetString("label11", ci);
            label12.Text = rm.GetString("label12", ci);
            label13.Text = rm.GetString("label13", ci);
            label14.Text = rm.GetString("label14", ci);
            label15.Text = rm.GetString("label15", ci);
            label16.Text = rm.GetString("label16", ci);
            label17.Text = rm.GetString("label17", ci);
            label18.Text = rm.GetString("label18", ci);
            label19.Text = rm.GetString("label19", ci);
            label20.Text = rm.GetString("label20", ci);
            label21.Text = rm.GetString("label21", ci);
            label22.Text = rm.GetString("label22", ci);
            label23.Text = rm.GetString("label23", ci);
            label24.Text = rm.GetString("label24", ci);
            label25.Text = rm.GetString("label25", ci);
            label26.Text = rm.GetString("label26", ci);
            label27.Text = rm.GetString("label27", ci);
            label28.Text = rm.GetString("label28", ci);
            label29.Text = rm.GetString("label29", ci);
            label30.Text = rm.GetString("label30", ci);
            label31.Text = rm.GetString("label31", ci);
            label32.Text = rm.GetString("label32", ci);
            label33.Text = rm.GetString("label33", ci);
            label34.Text = rm.GetString("label34", ci);
            label35.Text = rm.GetString("label35", ci);
            label36.Text = rm.GetString("label36", ci);
            label37.Text = rm.GetString("label37", ci);
            label38.Text = rm.GetString("label38", ci);
            label39.Text = rm.GetString("label39", ci);
            label40.Text = rm.GetString("label40", ci);
            label41.Text = rm.GetString("label41", ci);
            label42.Text = rm.GetString("label42", ci);
            label43.Text = rm.GetString("label43", ci);
            label44.Text = rm.GetString("label44", ci);
            label45.Text = rm.GetString("label45", ci);
            label46.Text = rm.GetString("label46", ci);
            label47.Text = rm.GetString("label47", ci);
            label48.Text = rm.GetString("label48", ci);
            label49.Text = rm.GetString("label49", ci);
            label50.Text = rm.GetString("label50", ci);
            label51.Text = rm.GetString("label51", ci);
            label52.Text = rm.GetString("label52", ci);
            label53.Text = rm.GetString("label53", ci);
            label54.Text = rm.GetString("label54", ci);
            label55.Text = rm.GetString("label55", ci);
            label56.Text = rm.GetString("label56", ci);
            label57.Text = rm.GetString("label57", ci);
            label58.Text = rm.GetString("label58", ci);
            label59.Text = rm.GetString("label59", ci);
            label60.Text = rm.GetString("label60", ci);
            label61.Text = rm.GetString("label61", ci);
            label62.Text = rm.GetString("label62", ci);
            label63.Text = rm.GetString("label63", ci);
            label64.Text = rm.GetString("label64", ci);
            label65.Text = rm.GetString("label65", ci);
            label66.Text = rm.GetString("label66", ci);
            label67.Text = rm.GetString("label67", ci);
            label68.Text = rm.GetString("label68", ci);
            label69.Text = rm.GetString("label69", ci);
            label70.Text = rm.GetString("label70", ci);
            label71.Text = rm.GetString("label71", ci);
            label72.Text = rm.GetString("label72", ci);
            label73.Text = rm.GetString("label73", ci);
            label74.Text = rm.GetString("label74", ci);
            label75.Text = rm.GetString("label75", ci);
            label76.Text = rm.GetString("label76", ci);
            label77.Text = rm.GetString("label77", ci);
            label78.Text = rm.GetString("label78", ci);
            label79.Text = rm.GetString("label79", ci);
            label80.Text = rm.GetString("label80", ci);
            label81.Text = rm.GetString("label81", ci);
            label82.Text = rm.GetString("label82", ci);
            label83.Text = rm.GetString("label83", ci);
            label86.Text = rm.GetString("label86", ci);
            label87.Text = rm.GetString("label87", ci);
            label88.Text = rm.GetString("label88", ci);
            label89.Text = rm.GetString("label89", ci);
            label90.Text = rm.GetString("label90", ci);
            label91.Text = rm.GetString("label91", ci);
            label92.Text = rm.GetString("label92", ci);
            label93.Text = rm.GetString("label93", ci);
            label94.Text = rm.GetString("label94", ci);
            label95.Text = rm.GetString("label95", ci);
            label96.Text = rm.GetString("label96", ci);
            label97.Text = rm.GetString("label97", ci);
            label98.Text = rm.GetString("label98", ci);
            label99.Text = rm.GetString("label99", ci);
            label100.Text = rm.GetString("label100", ci);
            label101.Text = rm.GetString("label101", ci);
            label102.Text = rm.GetString("label102", ci);
            label103.Text = rm.GetString("label103", ci);
            label104.Text = rm.GetString("label104", ci);
            label105.Text = rm.GetString("label105", ci);
            label108.Text = rm.GetString("label108", ci);
            label109.Text = rm.GetString("label109", ci);
            label110.Text = rm.GetString("label110", ci);
            label111.Text = rm.GetString("label111", ci);
            label112.Text = rm.GetString("label112", ci);
            label113.Text = rm.GetString("label113", ci);
            label114.Text = rm.GetString("label114", ci);
            label117.Text = rm.GetString("label117", ci);
            label118.Text = rm.GetString("label118", ci);
            label119.Text = rm.GetString("label119", ci);
            label120.Text = rm.GetString("label120", ci);
            label121.Text = rm.GetString("label121", ci);
            label125.Text = rm.GetString("label125", ci);
            label126.Text = rm.GetString("label126", ci);
            label127.Text = rm.GetString("label127", ci);
            label128.Text = rm.GetString("label128", ci);
            textBox77.Text = rm.GetString("textBox77", ci);
            textBox99.Text = rm.GetString("textBox99", ci);
        }
    }
}
