﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homeit
{
    class NozzleCalculator
    {
        const int hundredMillion = 1000000000;

        public static float calculateE5(float A5, float B5, float C5, float D5)
        {
            // =PI()*(A5^2-(A5-B5*2)^2)/4*C5*D5/1000000000
            float E5 = (float)(Math.PI * (Math.Pow(A5, 2) - Math.Pow(A5 - B5 * 2, 2)) / 4 * C5 * D5 / NozzleCalculator.hundredMillion);
            return E5;
        }

        public static float calculateE7(float A7, float B7, float C7, float D7)
        {
            // =PI()*((A7+2*B7)^2-A7^2)/4*C7*D7/1000000000
            float E7 = (float)(Math.PI * (Math.Pow(A7 + 2 * B7, 2) - Math.Pow(A7, 2)) / 4 * C7 * D7 / NozzleCalculator.hundredMillion);
            return E7;
        }

        public static float calculateE9(float A9, float C9, float D9)
        {
            // =PI()*A9^2/4*C9*D9/1000000000
            float E9 = (float)(Math.PI * Math.Pow(A9, 2) * C9 * D9 / NozzleCalculator.hundredMillion);
            return E9;
        }

        /**
         * @A13 textBox16
         * @B13 textBox17
         * @F13 textBox21
         * @H13 textBox23
         */
        public static float calculateJ13(float A13, float B13, float F13, float H13)
        {
            // =PI()*(A13^2-(A13-B13*2)^2)/4*F13*H13/1000000000
            float J13 = (float)(Math.PI * (Math.Pow(A13, 2) - Math.Pow(A13 - B13 * 2, 2)) / 4 * F13 * H13 / NozzleCalculator.hundredMillion);
            return J13;
        }

        public static float calculateK13(float A13, float C13, float D13, float E13, float G13, float I13)
        {
            // =PI()*((A13+2*E13)^2-A13^2)/4*G13*I13/1000000000+PI()*((A13+2*C13)^2-(A13+E13*2)^2)/4*I13*G13*E13/D13/1000000000
            float K13_1 = (float)(Math.PI * (Math.Pow(A13 + 2 * E13, 2) - Math.Pow(A13, 2) / 4 * G13 * I13 / NozzleCalculator.hundredMillion));
            float K13_2 = (float)(Math.PI * (Math.Pow(A13 + 2 * C13, 2) - Math.Pow(A13 + E13 * 2, 2) / 4 * I13 * G13 * E13 / D13 / NozzleCalculator.hundredMillion));
            float K13 = K13_1 + K13_2;
            return K13;
        }

        public static float calculateH32(float A32, float B32, float D32, float E32, float F32)
        {
            // =D32-E32-((A32/2)^2-(F32+B32/2)^2)^0.5
            float H32 = D32 - E32 - (float)Math.Pow((float)Math.Pow(A32 / 2, 2) - (float)Math.Pow(F32 + B32 / 2, 2), 0.5);
            return H32;
        }

        public static float calculateI32(float B32, float C32, float G32, float H32)
        {
            // =PI()*(B32-C32)*C32*H32*G32/1000000000
            float I32 = (float)Math.PI * (B32 - C32) * C32 * H32 * G32 / NozzleCalculator.hundredMillion;
            return I32;
        }

        public static float calculateK32(float A32, float B32, float D32, float E32, float F32, float J32)
        {
            // =D32-E32-((A32/2+J32)^2-(F32-B32/2)^2)^0.5
            float K32 = D32 - E32 - (float)(Math.Pow(Math.Pow(A32 / 2 + J32, 2) - Math.Pow(F32 - B32 / 2, 2), 0.5));
            return K32;
        }

        public static float calculateG52(float A52, float B52, float D52, float E52)
        {
            // =D52-E52-((A52/2)^2-(B52/2)^2)^0.5
            float G52 = D52 - E52 - (float)(Math.Pow(Math.Pow(A52 / 2, 2) - Math.Pow(B52 / 2, 2), 0.5));
            return G52;
        }

        // @G52 is calculated in previous method
        public static float calculateH52(float B52, float C52, float F52, float G52)
        {
            // =PI()*(B52-C52)*C52*G52*F52/1000000000
            float H52 = (float)Math.PI * (B52 - C52) * C52 * G52 * F52 / NozzleCalculator.hundredMillion;
            return H52;
        }

        public static float calculateJ52(float A52, float D52, float E52, float I52)
        {
            // =D52-E52-A52/2-I52
            float H52 = D52 - E52 - A52 / 2 - I52;
            return H52;
        }

        public static float calculateH72(float A72, float B72, float D72, float E72, float F72, float J72)
        {
            // =D72-J72-E72-0.5*((A72/2)^2-(F72+B72/2)^2)^0.5
            double H72_1 = Math.Pow(A72 / 2, 2);
            double H72_2 = Math.Pow((F72 + B72 / 2), 2);
            float H72 = (float)(D72 - J72 - E72 - Math.Pow(H72_1 - H72_2, 0.5) * 0.5);
            return H72;
        }

        // @H72 is calculated in previous method
        public static float calculateI72(float B72, float C72, float G72, float H72)
        {
            // =PI()*(B72-C72)*C72*H72*G72/1000000000
            float I72 = (float)Math.PI * (B72 - C72) * C72 * H72 * G72 / NozzleCalculator.hundredMillion;
            return I72;
        }

        public static float calculateL72(float A72, float B72, float D72, float E72, float F72, float J72, float K72)
        {
            // =D72-E72-J72-((0.9*A72+K72)^2-(F72-B72/2)^2)^0.5+((0.9*A72)^2-(F72-B72/2)^2)^0.5-0.5*((A72/2)^2-(F72-B72/2)^2)^0.5
            float L72_1 = (float)Math.Pow(Math.Pow(0.9 * A72 + K72, 2) - Math.Pow(F72 - B72 / 2, 2), 0.5);
            float L72_2 = (float)Math.Pow(Math.Pow(0.9 * A72, 2) - Math.Pow(F72 - B72 / 2, 2), 0.5);
            float L72_3 = (float)(0.5 * (Math.Pow(Math.Pow(A72/2, 2) - Math.Pow(F72-B72/2, 2), 0.5)));
            float L72 = D72 - E72 - J72 - (L72_1 + L72_2 - L72_3);
            return L72;
        }
    }
}
