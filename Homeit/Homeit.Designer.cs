﻿namespace Homeit
{
    partial class Homeit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Homeit));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Nozzle = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.FixedTube = new System.Windows.Forms.TabPage();
            this.label87 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label125 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.textBox130 = new System.Windows.Forms.TextBox();
            this.textBox131 = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.textBox132 = new System.Windows.Forms.TextBox();
            this.textBox133 = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.textBox129 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.textBox127 = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.textBox128 = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.textBox126 = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.textBox125 = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.textBox121 = new System.Windows.Forms.TextBox();
            this.textBox122 = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.textBox123 = new System.Windows.Forms.TextBox();
            this.textBox124 = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox111 = new System.Windows.Forms.TextBox();
            this.textBox112 = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.switchLanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.中文ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.Nozzle.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.FixedTube.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Nozzle);
            this.tabControl1.Controls.Add(this.FixedTube);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // Nozzle
            // 
            resources.ApplyResources(this.Nozzle, "Nozzle");
            this.Nozzle.Controls.Add(this.groupBox5);
            this.Nozzle.Controls.Add(this.groupBox4);
            this.Nozzle.Controls.Add(this.groupBox2);
            this.Nozzle.Controls.Add(this.groupBox1);
            this.Nozzle.Controls.Add(this.groupBox3);
            this.Nozzle.Name = "Nozzle";
            this.Nozzle.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox61);
            this.groupBox5.Controls.Add(this.label61);
            this.groupBox5.Controls.Add(this.textBox60);
            this.groupBox5.Controls.Add(this.label60);
            this.groupBox5.Controls.Add(this.textBox38);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.textBox50);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.textBox51);
            this.groupBox5.Controls.Add(this.textBox52);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.textBox53);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.textBox54);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.textBox55);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.textBox56);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.textBox57);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Controls.Add(this.textBox58);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.textBox59);
            this.groupBox5.Controls.Add(this.label59);
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox61, "textBox61");
            this.textBox61.Name = "textBox61";
            // 
            // label61
            // 
            resources.ApplyResources(this.label61, "label61");
            this.label61.Name = "label61";
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox60, "textBox60");
            this.textBox60.Name = "textBox60";
            this.textBox60.Leave += new System.EventHandler(this.textBox60_Leave);
            // 
            // label60
            // 
            resources.ApplyResources(this.label60, "label60");
            this.label60.Name = "label60";
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox38, "textBox38");
            this.textBox38.Name = "textBox38";
            this.textBox38.Leave += new System.EventHandler(this.textBox38_Leave);
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox50, "textBox50");
            this.textBox50.Name = "textBox50";
            // 
            // label50
            // 
            resources.ApplyResources(this.label50, "label50");
            this.label50.Name = "label50";
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox51, "textBox51");
            this.textBox51.Name = "textBox51";
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox52, "textBox52");
            this.textBox52.Name = "textBox52";
            // 
            // label51
            // 
            resources.ApplyResources(this.label51, "label51");
            this.label51.Name = "label51";
            // 
            // label52
            // 
            resources.ApplyResources(this.label52, "label52");
            this.label52.Name = "label52";
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox53, "textBox53");
            this.textBox53.Name = "textBox53";
            this.textBox53.Leave += new System.EventHandler(this.textBox53_Leave);
            // 
            // label53
            // 
            resources.ApplyResources(this.label53, "label53");
            this.label53.Name = "label53";
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox54, "textBox54");
            this.textBox54.Name = "textBox54";
            this.textBox54.Leave += new System.EventHandler(this.textBox54_Leave);
            // 
            // label54
            // 
            resources.ApplyResources(this.label54, "label54");
            this.label54.Name = "label54";
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox55, "textBox55");
            this.textBox55.Name = "textBox55";
            this.textBox55.Leave += new System.EventHandler(this.textBox55_Leave);
            // 
            // label55
            // 
            resources.ApplyResources(this.label55, "label55");
            this.label55.Name = "label55";
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox56, "textBox56");
            this.textBox56.Name = "textBox56";
            this.textBox56.Leave += new System.EventHandler(this.textBox56_Leave);
            // 
            // label56
            // 
            resources.ApplyResources(this.label56, "label56");
            this.label56.Name = "label56";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox57, "textBox57");
            this.textBox57.Name = "textBox57";
            this.textBox57.Leave += new System.EventHandler(this.textBox57_Leave);
            // 
            // label57
            // 
            resources.ApplyResources(this.label57, "label57");
            this.label57.Name = "label57";
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox58, "textBox58");
            this.textBox58.Name = "textBox58";
            this.textBox58.Leave += new System.EventHandler(this.textBox58_Leave);
            // 
            // label58
            // 
            resources.ApplyResources(this.label58, "label58");
            this.label58.Name = "label58";
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox59, "textBox59");
            this.textBox59.Name = "textBox59";
            this.textBox59.Leave += new System.EventHandler(this.textBox59_Leave);
            // 
            // label59
            // 
            resources.ApplyResources(this.label59, "label59");
            this.label59.Name = "label59";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox39);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.textBox40);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.textBox41);
            this.groupBox4.Controls.Add(this.textBox42);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.textBox43);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.textBox44);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.textBox45);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.textBox46);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.textBox47);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.textBox48);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.textBox49);
            this.groupBox4.Controls.Add(this.label49);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox39, "textBox39");
            this.textBox39.Name = "textBox39";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.Name = "label39";
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox40, "textBox40");
            this.textBox40.Name = "textBox40";
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.Name = "label40";
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox41, "textBox41");
            this.textBox41.Name = "textBox41";
            this.textBox41.Leave += new System.EventHandler(this.textBox41_Leave);
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox42, "textBox42");
            this.textBox42.Name = "textBox42";
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.Name = "label41";
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox43, "textBox43");
            this.textBox43.Name = "textBox43";
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.Name = "label43";
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox44, "textBox44");
            this.textBox44.Name = "textBox44";
            this.textBox44.Leave += new System.EventHandler(this.textBox44_Leave);
            // 
            // label44
            // 
            resources.ApplyResources(this.label44, "label44");
            this.label44.Name = "label44";
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox45, "textBox45");
            this.textBox45.Name = "textBox45";
            this.textBox45.Leave += new System.EventHandler(this.textBox45_Leave);
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.Name = "label45";
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox46, "textBox46");
            this.textBox46.Name = "textBox46";
            this.textBox46.Leave += new System.EventHandler(this.textBox46_Leave);
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.Name = "label46";
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox47, "textBox47");
            this.textBox47.Name = "textBox47";
            this.textBox47.Leave += new System.EventHandler(this.textBox47_Leave);
            // 
            // label47
            // 
            resources.ApplyResources(this.label47, "label47");
            this.label47.Name = "label47";
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox48, "textBox48");
            this.textBox48.Name = "textBox48";
            this.textBox48.Leave += new System.EventHandler(this.textBox48_Leave);
            // 
            // label48
            // 
            resources.ApplyResources(this.label48, "label48");
            this.label48.Name = "label48";
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox49, "textBox49");
            this.textBox49.Name = "textBox49";
            this.textBox49.Leave += new System.EventHandler(this.textBox49_Leave);
            // 
            // label49
            // 
            resources.ApplyResources(this.label49, "label49");
            this.label49.Name = "label49";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox25);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.textBox24);
            this.groupBox2.Controls.Add(this.textBox23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.textBox22);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.textBox21);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.textBox20);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.textBox19);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.textBox18);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.textBox17);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.textBox16);
            this.groupBox2.Controls.Add(this.label16);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox25, "textBox25");
            this.textBox25.Name = "textBox25";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox24, "textBox24");
            this.textBox24.Name = "textBox24";
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox23, "textBox23");
            this.textBox23.Name = "textBox23";
            this.textBox23.Leave += new System.EventHandler(this.textBox23_Leave);
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox22, "textBox22");
            this.textBox22.Name = "textBox22";
            this.textBox22.Leave += new System.EventHandler(this.textBox22_Leave);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox21, "textBox21");
            this.textBox21.Name = "textBox21";
            this.textBox21.Leave += new System.EventHandler(this.textBox21_Leave);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox20, "textBox20");
            this.textBox20.Name = "textBox20";
            this.textBox20.Leave += new System.EventHandler(this.textBox20_Leave);
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox19, "textBox19");
            this.textBox19.Name = "textBox19";
            this.textBox19.Leave += new System.EventHandler(this.textBox19_Leave);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox18, "textBox18");
            this.textBox18.Name = "textBox18";
            this.textBox18.Leave += new System.EventHandler(this.textBox18_Leave);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox17, "textBox17");
            this.textBox17.Name = "textBox17";
            this.textBox17.Leave += new System.EventHandler(this.textBox17_Leave);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox16, "textBox16");
            this.textBox16.Name = "textBox16";
            this.textBox16.Leave += new System.EventHandler(this.textBox16_Leave);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBox14);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBox15);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox11, "textBox11");
            this.textBox11.Name = "textBox11";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox12, "textBox12");
            this.textBox12.Name = "textBox12";
            this.textBox12.Leave += new System.EventHandler(this.textBox12_Leave);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox13, "textBox13");
            this.textBox13.Name = "textBox13";
            this.textBox13.Leave += new System.EventHandler(this.textBox13_Leave);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox14, "textBox14");
            this.textBox14.Name = "textBox14";
            this.textBox14.Leave += new System.EventHandler(this.textBox14_Leave);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox15, "textBox15");
            this.textBox15.Name = "textBox15";
            this.textBox15.Leave += new System.EventHandler(this.textBox15_Leave);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox6, "textBox6");
            this.textBox6.Name = "textBox6";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox7, "textBox7");
            this.textBox7.Name = "textBox7";
            this.textBox7.Leave += new System.EventHandler(this.textBox7_Leave);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox8, "textBox8");
            this.textBox8.Name = "textBox8";
            this.textBox8.Leave += new System.EventHandler(this.textBox8_Leave);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox9, "textBox9");
            this.textBox9.Name = "textBox9";
            this.textBox9.Leave += new System.EventHandler(this.textBox9_Leave);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox10, "textBox10");
            this.textBox10.Name = "textBox10";
            this.textBox10.Leave += new System.EventHandler(this.textBox10_Leave);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox5, "textBox5");
            this.textBox5.Name = "textBox5";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox4, "textBox4");
            this.textBox4.Name = "textBox4";
            this.textBox4.Leave += new System.EventHandler(this.textBox4_Leave);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox3, "textBox3");
            this.textBox3.Name = "textBox3";
            this.textBox3.Leave += new System.EventHandler(this.textBox3_Leave);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox37);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.textBox36);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.textBox26);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.textBox27);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.textBox29);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.textBox30);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.textBox31);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.textBox32);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.textBox33);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.textBox34);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.textBox35);
            this.groupBox3.Controls.Add(this.label35);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox37, "textBox37");
            this.textBox37.Name = "textBox37";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox36, "textBox36");
            this.textBox36.Name = "textBox36";
            this.textBox36.Leave += new System.EventHandler(this.textBox36_Leave);
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox26, "textBox26");
            this.textBox26.Name = "textBox26";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox27, "textBox27");
            this.textBox27.Name = "textBox27";
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.textBox28, "textBox28");
            this.textBox28.Name = "textBox28";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox29, "textBox29");
            this.textBox29.Name = "textBox29";
            this.textBox29.Leave += new System.EventHandler(this.textBox29_Leave);
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox30, "textBox30");
            this.textBox30.Name = "textBox30";
            this.textBox30.Leave += new System.EventHandler(this.textBox30_Leave);
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox31, "textBox31");
            this.textBox31.Name = "textBox31";
            this.textBox31.Leave += new System.EventHandler(this.textBox31_Leave);
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.Name = "label31";
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox32, "textBox32");
            this.textBox32.Name = "textBox32";
            this.textBox32.Leave += new System.EventHandler(this.textBox32_Leave);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox33, "textBox33");
            this.textBox33.Name = "textBox33";
            this.textBox33.Leave += new System.EventHandler(this.textBox33_Leave);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox34, "textBox34");
            this.textBox34.Name = "textBox34";
            this.textBox34.Leave += new System.EventHandler(this.textBox34_Leave);
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.textBox35, "textBox35");
            this.textBox35.Name = "textBox35";
            this.textBox35.Leave += new System.EventHandler(this.textBox35_Leave);
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // FixedTube
            // 
            resources.ApplyResources(this.FixedTube, "FixedTube");
            this.FixedTube.Controls.Add(this.label87);
            this.FixedTube.Controls.Add(this.groupBox6);
            this.FixedTube.Name = "FixedTube";
            this.FixedTube.UseVisualStyleBackColor = true;
            // 
            // label87
            // 
            resources.ApplyResources(this.label87, "label87");
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Name = "label87";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label125);
            this.groupBox6.Controls.Add(this.label121);
            this.groupBox6.Controls.Add(this.label120);
            this.groupBox6.Controls.Add(this.textBox130);
            this.groupBox6.Controls.Add(this.textBox131);
            this.groupBox6.Controls.Add(this.label118);
            this.groupBox6.Controls.Add(this.textBox132);
            this.groupBox6.Controls.Add(this.textBox133);
            this.groupBox6.Controls.Add(this.label119);
            this.groupBox6.Controls.Add(this.textBox129);
            this.groupBox6.Controls.Add(this.label117);
            this.groupBox6.Controls.Add(this.textBox127);
            this.groupBox6.Controls.Add(this.label127);
            this.groupBox6.Controls.Add(this.textBox128);
            this.groupBox6.Controls.Add(this.label128);
            this.groupBox6.Controls.Add(this.textBox126);
            this.groupBox6.Controls.Add(this.label126);
            this.groupBox6.Controls.Add(this.textBox125);
            this.groupBox6.Controls.Add(this.label114);
            this.groupBox6.Controls.Add(this.textBox121);
            this.groupBox6.Controls.Add(this.textBox122);
            this.groupBox6.Controls.Add(this.label111);
            this.groupBox6.Controls.Add(this.textBox123);
            this.groupBox6.Controls.Add(this.textBox124);
            this.groupBox6.Controls.Add(this.label112);
            this.groupBox6.Controls.Add(this.textBox119);
            this.groupBox6.Controls.Add(this.textBox120);
            this.groupBox6.Controls.Add(this.label110);
            this.groupBox6.Controls.Add(this.textBox117);
            this.groupBox6.Controls.Add(this.textBox118);
            this.groupBox6.Controls.Add(this.label109);
            this.groupBox6.Controls.Add(this.textBox115);
            this.groupBox6.Controls.Add(this.textBox116);
            this.groupBox6.Controls.Add(this.label108);
            this.groupBox6.Controls.Add(this.textBox114);
            this.groupBox6.Controls.Add(this.textBox111);
            this.groupBox6.Controls.Add(this.textBox112);
            this.groupBox6.Controls.Add(this.label105);
            this.groupBox6.Controls.Add(this.textBox113);
            this.groupBox6.Controls.Add(this.label113);
            this.groupBox6.Controls.Add(this.textBox110);
            this.groupBox6.Controls.Add(this.label104);
            this.groupBox6.Controls.Add(this.textBox109);
            this.groupBox6.Controls.Add(this.label103);
            this.groupBox6.Controls.Add(this.textBox107);
            this.groupBox6.Controls.Add(this.textBox108);
            this.groupBox6.Controls.Add(this.label102);
            this.groupBox6.Controls.Add(this.textBox105);
            this.groupBox6.Controls.Add(this.textBox106);
            this.groupBox6.Controls.Add(this.label101);
            this.groupBox6.Controls.Add(this.textBox104);
            this.groupBox6.Controls.Add(this.label100);
            this.groupBox6.Controls.Add(this.textBox103);
            this.groupBox6.Controls.Add(this.label99);
            this.groupBox6.Controls.Add(this.textBox102);
            this.groupBox6.Controls.Add(this.label98);
            this.groupBox6.Controls.Add(this.textBox101);
            this.groupBox6.Controls.Add(this.textBox100);
            this.groupBox6.Controls.Add(this.label97);
            this.groupBox6.Controls.Add(this.textBox93);
            this.groupBox6.Controls.Add(this.textBox94);
            this.groupBox6.Controls.Add(this.textBox95);
            this.groupBox6.Controls.Add(this.textBox96);
            this.groupBox6.Controls.Add(this.textBox97);
            this.groupBox6.Controls.Add(this.textBox98);
            this.groupBox6.Controls.Add(this.textBox99);
            this.groupBox6.Controls.Add(this.textBox92);
            this.groupBox6.Controls.Add(this.label96);
            this.groupBox6.Controls.Add(this.textBox91);
            this.groupBox6.Controls.Add(this.label95);
            this.groupBox6.Controls.Add(this.textBox90);
            this.groupBox6.Controls.Add(this.label94);
            this.groupBox6.Controls.Add(this.textBox89);
            this.groupBox6.Controls.Add(this.label93);
            this.groupBox6.Controls.Add(this.textBox88);
            this.groupBox6.Controls.Add(this.label92);
            this.groupBox6.Controls.Add(this.textBox87);
            this.groupBox6.Controls.Add(this.label91);
            this.groupBox6.Controls.Add(this.textBox86);
            this.groupBox6.Controls.Add(this.label90);
            this.groupBox6.Controls.Add(this.textBox85);
            this.groupBox6.Controls.Add(this.label89);
            this.groupBox6.Controls.Add(this.textBox84);
            this.groupBox6.Controls.Add(this.label88);
            this.groupBox6.Controls.Add(this.label86);
            this.groupBox6.Controls.Add(this.textBox83);
            this.groupBox6.Controls.Add(this.panel27);
            this.groupBox6.Controls.Add(this.textBox82);
            this.groupBox6.Controls.Add(this.panel26);
            this.groupBox6.Controls.Add(this.textBox81);
            this.groupBox6.Controls.Add(this.label83);
            this.groupBox6.Controls.Add(this.panel25);
            this.groupBox6.Controls.Add(this.textBox80);
            this.groupBox6.Controls.Add(this.label82);
            this.groupBox6.Controls.Add(this.panel24);
            this.groupBox6.Controls.Add(this.textBox62);
            this.groupBox6.Controls.Add(this.label81);
            this.groupBox6.Controls.Add(this.panel23);
            this.groupBox6.Controls.Add(this.textBox79);
            this.groupBox6.Controls.Add(this.textBox78);
            this.groupBox6.Controls.Add(this.label80);
            this.groupBox6.Controls.Add(this.panel22);
            this.groupBox6.Controls.Add(this.textBox77);
            this.groupBox6.Controls.Add(this.label79);
            this.groupBox6.Controls.Add(this.panel21);
            this.groupBox6.Controls.Add(this.panel20);
            this.groupBox6.Controls.Add(this.textBox76);
            this.groupBox6.Controls.Add(this.label78);
            this.groupBox6.Controls.Add(this.panel19);
            this.groupBox6.Controls.Add(this.textBox75);
            this.groupBox6.Controls.Add(this.label77);
            this.groupBox6.Controls.Add(this.panel18);
            this.groupBox6.Controls.Add(this.textBox74);
            this.groupBox6.Controls.Add(this.label76);
            this.groupBox6.Controls.Add(this.panel17);
            this.groupBox6.Controls.Add(this.textBox73);
            this.groupBox6.Controls.Add(this.label75);
            this.groupBox6.Controls.Add(this.panel16);
            this.groupBox6.Controls.Add(this.textBox72);
            this.groupBox6.Controls.Add(this.label74);
            this.groupBox6.Controls.Add(this.panel15);
            this.groupBox6.Controls.Add(this.label73);
            this.groupBox6.Controls.Add(this.panel14);
            this.groupBox6.Controls.Add(this.panel13);
            this.groupBox6.Controls.Add(this.panel12);
            this.groupBox6.Controls.Add(this.panel11);
            this.groupBox6.Controls.Add(this.textBox71);
            this.groupBox6.Controls.Add(this.panel10);
            this.groupBox6.Controls.Add(this.label72);
            this.groupBox6.Controls.Add(this.textBox70);
            this.groupBox6.Controls.Add(this.panel9);
            this.groupBox6.Controls.Add(this.label71);
            this.groupBox6.Controls.Add(this.textBox69);
            this.groupBox6.Controls.Add(this.panel8);
            this.groupBox6.Controls.Add(this.label70);
            this.groupBox6.Controls.Add(this.textBox68);
            this.groupBox6.Controls.Add(this.panel7);
            this.groupBox6.Controls.Add(this.label69);
            this.groupBox6.Controls.Add(this.textBox67);
            this.groupBox6.Controls.Add(this.panel6);
            this.groupBox6.Controls.Add(this.label68);
            this.groupBox6.Controls.Add(this.textBox66);
            this.groupBox6.Controls.Add(this.panel5);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Controls.Add(this.textBox65);
            this.groupBox6.Controls.Add(this.panel4);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Controls.Add(this.textBox64);
            this.groupBox6.Controls.Add(this.panel3);
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.textBox63);
            this.groupBox6.Controls.Add(this.panel2);
            this.groupBox6.Controls.Add(this.label64);
            this.groupBox6.Controls.Add(this.label63);
            this.groupBox6.Controls.Add(this.panel1);
            this.groupBox6.Controls.Add(this.label62);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            this.groupBox6.UseCompatibleTextRendering = true;
            // 
            // label125
            // 
            resources.ApplyResources(this.label125, "label125");
            this.label125.Name = "label125";
            // 
            // label121
            // 
            resources.ApplyResources(this.label121, "label121");
            this.label121.Name = "label121";
            // 
            // label120
            // 
            resources.ApplyResources(this.label120, "label120");
            this.label120.Name = "label120";
            // 
            // textBox130
            // 
            resources.ApplyResources(this.textBox130, "textBox130");
            this.textBox130.Name = "textBox130";
            this.textBox130.ReadOnly = true;
            // 
            // textBox131
            // 
            resources.ApplyResources(this.textBox131, "textBox131");
            this.textBox131.Name = "textBox131";
            this.textBox131.ReadOnly = true;
            // 
            // label118
            // 
            resources.ApplyResources(this.label118, "label118");
            this.label118.Name = "label118";
            // 
            // textBox132
            // 
            resources.ApplyResources(this.textBox132, "textBox132");
            this.textBox132.Name = "textBox132";
            this.textBox132.ReadOnly = true;
            // 
            // textBox133
            // 
            resources.ApplyResources(this.textBox133, "textBox133");
            this.textBox133.Name = "textBox133";
            this.textBox133.ReadOnly = true;
            // 
            // label119
            // 
            resources.ApplyResources(this.label119, "label119");
            this.label119.Name = "label119";
            // 
            // textBox129
            // 
            resources.ApplyResources(this.textBox129, "textBox129");
            this.textBox129.Name = "textBox129";
            this.textBox129.ReadOnly = true;
            // 
            // label117
            // 
            resources.ApplyResources(this.label117, "label117");
            this.label117.Name = "label117";
            // 
            // textBox127
            // 
            this.textBox127.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox127, "textBox127");
            this.textBox127.Name = "textBox127";
            this.textBox127.Leave += new System.EventHandler(this.textBox127_Leave);
            // 
            // label127
            // 
            resources.ApplyResources(this.label127, "label127");
            this.label127.Name = "label127";
            // 
            // textBox128
            // 
            this.textBox128.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox128, "textBox128");
            this.textBox128.Name = "textBox128";
            this.textBox128.Leave += new System.EventHandler(this.textBox128_Leave);
            // 
            // label128
            // 
            resources.ApplyResources(this.label128, "label128");
            this.label128.Name = "label128";
            // 
            // textBox126
            // 
            this.textBox126.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox126, "textBox126");
            this.textBox126.Name = "textBox126";
            this.textBox126.Leave += new System.EventHandler(this.textBox126_Leave);
            // 
            // label126
            // 
            resources.ApplyResources(this.label126, "label126");
            this.label126.Name = "label126";
            // 
            // textBox125
            // 
            this.textBox125.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox125, "textBox125");
            this.textBox125.Name = "textBox125";
            this.textBox125.Leave += new System.EventHandler(this.textBox125_Leave);
            // 
            // label114
            // 
            resources.ApplyResources(this.label114, "label114");
            this.label114.Name = "label114";
            // 
            // textBox121
            // 
            resources.ApplyResources(this.textBox121, "textBox121");
            this.textBox121.Name = "textBox121";
            this.textBox121.ReadOnly = true;
            // 
            // textBox122
            // 
            resources.ApplyResources(this.textBox122, "textBox122");
            this.textBox122.Name = "textBox122";
            this.textBox122.ReadOnly = true;
            // 
            // label111
            // 
            resources.ApplyResources(this.label111, "label111");
            this.label111.Name = "label111";
            // 
            // textBox123
            // 
            resources.ApplyResources(this.textBox123, "textBox123");
            this.textBox123.Name = "textBox123";
            this.textBox123.ReadOnly = true;
            // 
            // textBox124
            // 
            resources.ApplyResources(this.textBox124, "textBox124");
            this.textBox124.Name = "textBox124";
            this.textBox124.ReadOnly = true;
            // 
            // label112
            // 
            resources.ApplyResources(this.label112, "label112");
            this.label112.Name = "label112";
            // 
            // textBox119
            // 
            resources.ApplyResources(this.textBox119, "textBox119");
            this.textBox119.Name = "textBox119";
            this.textBox119.ReadOnly = true;
            // 
            // textBox120
            // 
            resources.ApplyResources(this.textBox120, "textBox120");
            this.textBox120.Name = "textBox120";
            this.textBox120.ReadOnly = true;
            // 
            // label110
            // 
            resources.ApplyResources(this.label110, "label110");
            this.label110.Name = "label110";
            // 
            // textBox117
            // 
            resources.ApplyResources(this.textBox117, "textBox117");
            this.textBox117.Name = "textBox117";
            this.textBox117.ReadOnly = true;
            // 
            // textBox118
            // 
            resources.ApplyResources(this.textBox118, "textBox118");
            this.textBox118.Name = "textBox118";
            this.textBox118.ReadOnly = true;
            // 
            // label109
            // 
            resources.ApplyResources(this.label109, "label109");
            this.label109.Name = "label109";
            // 
            // textBox115
            // 
            resources.ApplyResources(this.textBox115, "textBox115");
            this.textBox115.Name = "textBox115";
            this.textBox115.ReadOnly = true;
            // 
            // textBox116
            // 
            resources.ApplyResources(this.textBox116, "textBox116");
            this.textBox116.Name = "textBox116";
            this.textBox116.ReadOnly = true;
            // 
            // label108
            // 
            resources.ApplyResources(this.label108, "label108");
            this.label108.Name = "label108";
            // 
            // textBox114
            // 
            this.textBox114.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox114, "textBox114");
            this.textBox114.Name = "textBox114";
            this.textBox114.Leave += new System.EventHandler(this.textBox114_Leave);
            // 
            // textBox111
            // 
            resources.ApplyResources(this.textBox111, "textBox111");
            this.textBox111.Name = "textBox111";
            this.textBox111.ReadOnly = true;
            // 
            // textBox112
            // 
            resources.ApplyResources(this.textBox112, "textBox112");
            this.textBox112.Name = "textBox112";
            this.textBox112.ReadOnly = true;
            // 
            // label105
            // 
            resources.ApplyResources(this.label105, "label105");
            this.label105.Name = "label105";
            // 
            // textBox113
            // 
            this.textBox113.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox113, "textBox113");
            this.textBox113.Name = "textBox113";
            this.textBox113.Leave += new System.EventHandler(this.textBox113_Leave);
            // 
            // label113
            // 
            resources.ApplyResources(this.label113, "label113");
            this.label113.Name = "label113";
            // 
            // textBox110
            // 
            resources.ApplyResources(this.textBox110, "textBox110");
            this.textBox110.Name = "textBox110";
            this.textBox110.ReadOnly = true;
            // 
            // label104
            // 
            resources.ApplyResources(this.label104, "label104");
            this.label104.Name = "label104";
            // 
            // textBox109
            // 
            this.textBox109.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox109, "textBox109");
            this.textBox109.Name = "textBox109";
            this.textBox109.Leave += new System.EventHandler(this.textBox109_Leave);
            // 
            // label103
            // 
            resources.ApplyResources(this.label103, "label103");
            this.label103.Name = "label103";
            // 
            // textBox107
            // 
            resources.ApplyResources(this.textBox107, "textBox107");
            this.textBox107.Name = "textBox107";
            this.textBox107.ReadOnly = true;
            // 
            // textBox108
            // 
            resources.ApplyResources(this.textBox108, "textBox108");
            this.textBox108.Name = "textBox108";
            this.textBox108.ReadOnly = true;
            // 
            // label102
            // 
            resources.ApplyResources(this.label102, "label102");
            this.label102.Name = "label102";
            // 
            // textBox105
            // 
            resources.ApplyResources(this.textBox105, "textBox105");
            this.textBox105.Name = "textBox105";
            this.textBox105.ReadOnly = true;
            // 
            // textBox106
            // 
            resources.ApplyResources(this.textBox106, "textBox106");
            this.textBox106.Name = "textBox106";
            this.textBox106.ReadOnly = true;
            // 
            // label101
            // 
            resources.ApplyResources(this.label101, "label101");
            this.label101.Name = "label101";
            // 
            // textBox104
            // 
            resources.ApplyResources(this.textBox104, "textBox104");
            this.textBox104.Name = "textBox104";
            this.textBox104.ReadOnly = true;
            // 
            // label100
            // 
            resources.ApplyResources(this.label100, "label100");
            this.label100.Name = "label100";
            // 
            // textBox103
            // 
            resources.ApplyResources(this.textBox103, "textBox103");
            this.textBox103.Name = "textBox103";
            this.textBox103.ReadOnly = true;
            // 
            // label99
            // 
            resources.ApplyResources(this.label99, "label99");
            this.label99.Name = "label99";
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox102, "textBox102");
            this.textBox102.Name = "textBox102";
            this.textBox102.Leave += new System.EventHandler(this.textBox102_Leave);
            // 
            // label98
            // 
            resources.ApplyResources(this.label98, "label98");
            this.label98.Name = "label98";
            // 
            // textBox101
            // 
            resources.ApplyResources(this.textBox101, "textBox101");
            this.textBox101.Name = "textBox101";
            this.textBox101.ReadOnly = true;
            // 
            // textBox100
            // 
            resources.ApplyResources(this.textBox100, "textBox100");
            this.textBox100.Name = "textBox100";
            this.textBox100.ReadOnly = true;
            // 
            // label97
            // 
            resources.ApplyResources(this.label97, "label97");
            this.label97.Name = "label97";
            // 
            // textBox93
            // 
            this.textBox93.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox93, "textBox93");
            this.textBox93.Name = "textBox93";
            this.textBox93.ReadOnly = true;
            this.textBox93.TabStop = false;
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox94, "textBox94");
            this.textBox94.Name = "textBox94";
            this.textBox94.ReadOnly = true;
            this.textBox94.TabStop = false;
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox95, "textBox95");
            this.textBox95.Name = "textBox95";
            this.textBox95.ReadOnly = true;
            this.textBox95.TabStop = false;
            // 
            // textBox96
            // 
            this.textBox96.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox96, "textBox96");
            this.textBox96.Name = "textBox96";
            this.textBox96.ReadOnly = true;
            this.textBox96.TabStop = false;
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox97, "textBox97");
            this.textBox97.Name = "textBox97";
            this.textBox97.ReadOnly = true;
            this.textBox97.TabStop = false;
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.Color.LimeGreen;
            resources.ApplyResources(this.textBox98, "textBox98");
            this.textBox98.Name = "textBox98";
            this.textBox98.ReadOnly = true;
            this.textBox98.TabStop = false;
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.Color.White;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.textBox99, "textBox99");
            this.textBox99.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBox99.Name = "textBox99";
            this.textBox99.ReadOnly = true;
            this.textBox99.TabStop = false;
            // 
            // textBox92
            // 
            resources.ApplyResources(this.textBox92, "textBox92");
            this.textBox92.Name = "textBox92";
            this.textBox92.ReadOnly = true;
            // 
            // label96
            // 
            resources.ApplyResources(this.label96, "label96");
            this.label96.Name = "label96";
            // 
            // textBox91
            // 
            resources.ApplyResources(this.textBox91, "textBox91");
            this.textBox91.Name = "textBox91";
            this.textBox91.ReadOnly = true;
            // 
            // label95
            // 
            resources.ApplyResources(this.label95, "label95");
            this.label95.Name = "label95";
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.textBox90, "textBox90");
            this.textBox90.Name = "textBox90";
            this.textBox90.ReadOnly = true;
            // 
            // label94
            // 
            resources.ApplyResources(this.label94, "label94");
            this.label94.Name = "label94";
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.textBox89, "textBox89");
            this.textBox89.Name = "textBox89";
            this.textBox89.ReadOnly = true;
            // 
            // label93
            // 
            resources.ApplyResources(this.label93, "label93");
            this.label93.Name = "label93";
            // 
            // textBox88
            // 
            resources.ApplyResources(this.textBox88, "textBox88");
            this.textBox88.Name = "textBox88";
            this.textBox88.ReadOnly = true;
            // 
            // label92
            // 
            resources.ApplyResources(this.label92, "label92");
            this.label92.Name = "label92";
            // 
            // textBox87
            // 
            resources.ApplyResources(this.textBox87, "textBox87");
            this.textBox87.Name = "textBox87";
            this.textBox87.ReadOnly = true;
            // 
            // label91
            // 
            resources.ApplyResources(this.label91, "label91");
            this.label91.Name = "label91";
            // 
            // textBox86
            // 
            resources.ApplyResources(this.textBox86, "textBox86");
            this.textBox86.Name = "textBox86";
            this.textBox86.ReadOnly = true;
            // 
            // label90
            // 
            resources.ApplyResources(this.label90, "label90");
            this.label90.Name = "label90";
            // 
            // textBox85
            // 
            resources.ApplyResources(this.textBox85, "textBox85");
            this.textBox85.Name = "textBox85";
            this.textBox85.ReadOnly = true;
            // 
            // label89
            // 
            resources.ApplyResources(this.label89, "label89");
            this.label89.Name = "label89";
            // 
            // textBox84
            // 
            resources.ApplyResources(this.textBox84, "textBox84");
            this.textBox84.Name = "textBox84";
            this.textBox84.ReadOnly = true;
            // 
            // label88
            // 
            resources.ApplyResources(this.label88, "label88");
            this.label88.Name = "label88";
            // 
            // label86
            // 
            resources.ApplyResources(this.label86, "label86");
            this.label86.Name = "label86";
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox83, "textBox83");
            this.textBox83.Name = "textBox83";
            this.textBox83.ReadOnly = true;
            this.textBox83.TabStop = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel27, "panel27");
            this.panel27.Name = "panel27";
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox82, "textBox82");
            this.textBox82.Name = "textBox82";
            this.textBox82.ReadOnly = true;
            this.textBox82.TabStop = false;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel26, "panel26");
            this.panel26.Name = "panel26";
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox81, "textBox81");
            this.textBox81.Name = "textBox81";
            this.textBox81.ReadOnly = true;
            this.textBox81.TabStop = false;
            // 
            // label83
            // 
            resources.ApplyResources(this.label83, "label83");
            this.label83.Name = "label83";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel25, "panel25");
            this.panel25.Name = "panel25";
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox80, "textBox80");
            this.textBox80.Name = "textBox80";
            this.textBox80.ReadOnly = true;
            this.textBox80.TabStop = false;
            // 
            // label82
            // 
            resources.ApplyResources(this.label82, "label82");
            this.label82.Name = "label82";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel24, "panel24");
            this.panel24.Name = "panel24";
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox62, "textBox62");
            this.textBox62.Name = "textBox62";
            this.textBox62.ReadOnly = true;
            this.textBox62.TabStop = false;
            // 
            // label81
            // 
            resources.ApplyResources(this.label81, "label81");
            this.label81.Name = "label81";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel23, "panel23");
            this.panel23.Name = "panel23";
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox79, "textBox79");
            this.textBox79.Name = "textBox79";
            this.textBox79.Leave += new System.EventHandler(this.textBox79_Leave);
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.Color.Fuchsia;
            resources.ApplyResources(this.textBox78, "textBox78");
            this.textBox78.Name = "textBox78";
            this.textBox78.ReadOnly = true;
            this.textBox78.TabStop = false;
            // 
            // label80
            // 
            resources.ApplyResources(this.label80, "label80");
            this.label80.Name = "label80";
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel22, "panel22");
            this.panel22.Name = "panel22";
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.Color.White;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.textBox77, "textBox77");
            this.textBox77.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBox77.Name = "textBox77";
            this.textBox77.ReadOnly = true;
            this.textBox77.TabStop = false;
            // 
            // label79
            // 
            resources.ApplyResources(this.label79, "label79");
            this.label79.Name = "label79";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel21, "panel21");
            this.panel21.Name = "panel21";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // textBox76
            // 
            resources.ApplyResources(this.textBox76, "textBox76");
            this.textBox76.Name = "textBox76";
            this.textBox76.ReadOnly = true;
            // 
            // label78
            // 
            resources.ApplyResources(this.label78, "label78");
            this.label78.Name = "label78";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // textBox75
            // 
            resources.ApplyResources(this.textBox75, "textBox75");
            this.textBox75.Name = "textBox75";
            this.textBox75.ReadOnly = true;
            // 
            // label77
            // 
            resources.ApplyResources(this.label77, "label77");
            this.label77.Name = "label77";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // textBox74
            // 
            resources.ApplyResources(this.textBox74, "textBox74");
            this.textBox74.Name = "textBox74";
            this.textBox74.ReadOnly = true;
            // 
            // label76
            // 
            resources.ApplyResources(this.label76, "label76");
            this.label76.Name = "label76";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // textBox73
            // 
            resources.ApplyResources(this.textBox73, "textBox73");
            this.textBox73.Name = "textBox73";
            this.textBox73.ReadOnly = true;
            // 
            // label75
            // 
            resources.ApplyResources(this.label75, "label75");
            this.label75.Name = "label75";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // textBox72
            // 
            resources.ApplyResources(this.textBox72, "textBox72");
            this.textBox72.Name = "textBox72";
            this.textBox72.ReadOnly = true;
            // 
            // label74
            // 
            resources.ApplyResources(this.label74, "label74");
            this.label74.Name = "label74";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // label73
            // 
            resources.ApplyResources(this.label73, "label73");
            this.label73.Name = "label73";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel14, "panel14");
            this.panel14.Name = "panel14";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel11, "panel11");
            this.panel11.Name = "panel11";
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox71, "textBox71");
            this.textBox71.Name = "textBox71";
            this.textBox71.Leave += new System.EventHandler(this.textBox71_Leave);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel10, "panel10");
            this.panel10.Name = "panel10";
            // 
            // label72
            // 
            resources.ApplyResources(this.label72, "label72");
            this.label72.Name = "label72";
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox70, "textBox70");
            this.textBox70.Name = "textBox70";
            this.textBox70.Leave += new System.EventHandler(this.textBox70_Leave);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel9, "panel9");
            this.panel9.Name = "panel9";
            // 
            // label71
            // 
            resources.ApplyResources(this.label71, "label71");
            this.label71.Name = "label71";
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox69, "textBox69");
            this.textBox69.Name = "textBox69";
            this.textBox69.Leave += new System.EventHandler(this.textBox69_Leave);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Name = "panel8";
            // 
            // label70
            // 
            resources.ApplyResources(this.label70, "label70");
            this.label70.Name = "label70";
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox68, "textBox68");
            this.textBox68.Name = "textBox68";
            this.textBox68.Leave += new System.EventHandler(this.textBox68_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.Name = "panel7";
            // 
            // label69
            // 
            resources.ApplyResources(this.label69, "label69");
            this.label69.Name = "label69";
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox67, "textBox67");
            this.textBox67.Name = "textBox67";
            this.textBox67.Leave += new System.EventHandler(this.textBox67_Leave);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // label68
            // 
            resources.ApplyResources(this.label68, "label68");
            this.label68.Name = "label68";
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox66, "textBox66");
            this.textBox66.Name = "textBox66";
            this.textBox66.Leave += new System.EventHandler(this.textBox66_Leave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // label67
            // 
            resources.ApplyResources(this.label67, "label67");
            this.label67.Name = "label67";
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox65, "textBox65");
            this.textBox65.Name = "textBox65";
            this.textBox65.Leave += new System.EventHandler(this.textBox65_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label66
            // 
            resources.ApplyResources(this.label66, "label66");
            this.label66.Name = "label66";
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox64, "textBox64");
            this.textBox64.Name = "textBox64";
            this.textBox64.Leave += new System.EventHandler(this.textBox64_Leave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // label65
            // 
            resources.ApplyResources(this.label65, "label65");
            this.label65.Name = "label65";
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.Color.LightSkyBlue;
            resources.ApplyResources(this.textBox63, "textBox63");
            this.textBox63.Name = "textBox63";
            this.textBox63.Leave += new System.EventHandler(this.textBox63_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // label64
            // 
            resources.ApplyResources(this.label64, "label64");
            this.label64.Name = "label64";
            // 
            // label63
            // 
            resources.ApplyResources(this.label63, "label63");
            this.label63.Name = "label63";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ScrollBar;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // label62
            // 
            resources.ApplyResources(this.label62, "label62");
            this.label62.Name = "label62";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.switchLanguageToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // switchLanguageToolStripMenuItem
            // 
            this.switchLanguageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToolStripMenuItem,
            this.中文ToolStripMenuItem});
            this.switchLanguageToolStripMenuItem.Name = "switchLanguageToolStripMenuItem";
            resources.ApplyResources(this.switchLanguageToolStripMenuItem, "switchLanguageToolStripMenuItem");
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Checked = true;
            this.englishToolStripMenuItem.CheckOnClick = true;
            this.englishToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            resources.ApplyResources(this.englishToolStripMenuItem, "englishToolStripMenuItem");
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.englishToolStripMenuItem_Click);
            // 
            // 中文ToolStripMenuItem
            // 
            this.中文ToolStripMenuItem.CheckOnClick = true;
            this.中文ToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Replace;
            this.中文ToolStripMenuItem.Name = "中文ToolStripMenuItem";
            resources.ApplyResources(this.中文ToolStripMenuItem, "中文ToolStripMenuItem");
            this.中文ToolStripMenuItem.Click += new System.EventHandler(this.中文ToolStripMenuItem_Click);
            // 
            // Homeit
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Homeit";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Load += new System.EventHandler(this.Homeit_Load);
            this.tabControl1.ResumeLayout(false);
            this.Nozzle.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.FixedTube.ResumeLayout(false);
            this.FixedTube.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Nozzle;
        private System.Windows.Forms.TabPage FixedTube;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox111;
        private System.Windows.Forms.TextBox textBox112;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox textBox127;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox textBox128;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox textBox126;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.TextBox textBox125;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox textBox121;
        private System.Windows.Forms.TextBox textBox122;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox textBox123;
        private System.Windows.Forms.TextBox textBox124;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox textBox129;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox textBox130;
        private System.Windows.Forms.TextBox textBox131;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox textBox132;
        private System.Windows.Forms.TextBox textBox133;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem switchLanguageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 中文ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
    }
}

