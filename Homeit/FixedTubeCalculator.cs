﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homeit
{
    class FixedTubeCalculator
    {
        public static float calculateC16(float C4, float C12)
        {
            // =PI()*(C4/2*0.001)^2*(2*(C4/4)*0.001/3+C12*0.001)
            float C16 = (float)(Math.PI * Math.Pow((C4 / 2 * 0.001), 2) * (2 * (C4 / 4) * 0.001 / 3 + C12 * 0.001));
            return C16;
        }

        public static float calculateC17(float C4, float C5)
        {
            // =PI()*(C4/2*0.001)^2*(C5*0.001)
            float C17 = (float) (Math.PI * Math.Pow((C4/ 2 * 0.001), 2) *(C5 * 0.001));
            return C17;
        }

        public static float calculateC18(float C16, float C17)
        {
            // =2*(C16+C17)
            float C18 = 2 * (C16 + C17);
            return C18;
        }

        public static float calculateC19(float C6, float C7, float C8, float C9)
        {
            // =PI()*((C6-2*C7)/2*0.001)^2*(C9*0.001)*C8
            float C19 = (float)(Math.PI * Math.Pow(((C6 - 2 * C7) / 2 * 0.001), 2) * (C9 * 0.001) * C8);
            return C19;
        }

        /**
         * @C19 is calculated in previous method
         */
        public static float calculateC20(float C16, float C17, float C19)
        {
            // =2*(C16+C17)+C19
            float C20 = 2 * (C16 + C17) + C19;
            return C20;
        }

        public static float calculateC23(float H6, float H7)
        {
            // =(H6+H7)*2
            float C23 = (H6 + H7) * 2;
            return C23;
        }

        public static float calculateC24(float H19)
        {
            // =H19
            float C24 = H19;
            return C24;
        }

        public static float calculateC25(float H15)
        {
            // =H15*2
            float C25 = H15 * 2;
            return C25;
        }

        public static float calculateC26(float H12)
        {
            // =H12
            float C26 = H12;
            return C26;
        }

        public static float calculateC27(float H28)
        {
            // =H28
            float C27 = H28;
            return C27;
        }

        public static float calculateC28(float C23, float C24, float C25, float C26, float C27)
        {
            // =SUM(C23:C27)
            float C28 = C23 + C24 + C25 + C26 + C27;
            return C28;
        }

        public static float calculateE4(float E20)
        {
            // =E20
            float E4 = E20;
            return E4;
        }

        public static float calculateE7(float C20)
        {
            // =C20
            float E7 = C20;
            return E7;
        }

        public static float calculateE10(float E4, float E7)
        {
            // =E7+E4
            float E10 = 0;
            return E10;
        }

        public static float calculateE13(float C6, float C8, float E18)
        {
            // =PI()*C6*0.001*E18*0.001*C8
            float E13 = (float)(Math.PI * C6 * 0.001 * E18 * 0.001 * C8);
            return E13;
        }

        public static float calculateE16(float C9, float C10, float C11)
        {
            // =C9-2*(C11+C10)
            float E16 = C9 - 2 * (C11 + C10);
            return E16;
        }

        /**
         * @E16 is calculated in previous method
         */
        public static float calculateE17(float C3, float E16)
        {
            // =PI()*(C3/2*0.001)^2*(E16*0.001)
            float E17 = (float)(Math.PI * Math.Pow((C3 / 2 * 0.001), 2) * (E16 * 0.001));
            return E17;
        }

        public static float calculateE18(float C9, float C10, float C11)
        {
            // =C9-2*(C11+C10)
            float E18 = C9 - 2 * (C11 + C10);
            return E18;
        }

        /**
         * @E18 is calculated in previous method
         */
        public static float calculateE19(float C6, float C8, float E18)
        {
            // =PI()*(C6/2*0.001)^2*(E18*0.001)*C8
            float E19 = (float)(Math.PI * Math.Pow((C6 / 2 * 0.001), 2) * (E18 * 0.001) * C8);
            return E19;
        }

        /**
         * @E17 is calculated in previous method
         * @E19 is calculated in previous method
         */
        public static float calculateE20(float E17, float E19)
        {
            // =E17-E19
            float E20 = E17 - E19;
            return E20;
        }

        public static float calculateE23(float I6, float I7)
        {
            // =(I6+I7)*2
            float E23 = (I6 + I7) * 2;
            return E23;
        }

        public static float calculateE24(float I19)
        {
            // =I19
            float E24 = I19;
            return E24;
        }

        public static float calculateE25(float I15)
        {
            // =I15*2
            float E25 = I15 * 2;
            return E25;
        }

        public static float calculateE26(float I12)
        {
            // =I12
            float E26 = I12;
            return E26;
        }

        public static float calculateE27(float I28)
        {
            // =I28
            float E27 = I28;
            return E27;
        }

        public static float calculateE28(float E23, float E24, float E25, float E26, float E27)
        {
            // =SUM(E23:E27)
            float E28 = E23 + E24 + E25 + E26 + E27;
            return E28;
        }

        public static float calculateH4(float C4)
        {
            // =C4/2
            float H4 = C4 / 2;
            return H4;
        }

        public static float calculateH5(float C4)
        {
            // =C4/4
            float H5 = C4 / 4;
            return H5;
        }

        public static float calculateH6(float C12, float C16, float H2, float H3, float H4, float H5)
        {
            // =(PI()*((H4+H3)*0.001)^2*(2*(H5+H3)*0.001/3+C12*0.001)-C16)*H2*1000
            float H6 = (float)(Math.PI * Math.Pow(((H4 + H3) * 0.001), 2) * ((2 * (H5 + H3) * 0.001 / 3 + C12 * 0.001) - C16) * H2 * 1000);
            return H6;
        }

        public static float calculateI6(float C12, float C16, float H3, float H4, float H5, float I2)
        {
            // =(PI()*((H4+H3)*0.001)^2*(2*(H5+H3)*0.001/3+C12*0.001)-C16)*I2*1000
            float I6 = (float)(Math.PI * Math.Pow(((H4 + H3) * 0.001), 2) * ((2 * (H5 + H3) * 0.001 / 3 + C12 * 0.001) - C16) * I2 * 1000);
            return I6;
        }

        public static float calculateH7(float C4, float C5, float H2, float H3)
        {
            // =PI()*(C4+H3)*0.001*C5*0.001*H3*H2
            float H7 = (float)(Math.PI * (C4 + H3) * 0.001 * C5 * 0.001 * H3 * H2);
            return H7;
        }

        public static float calculateI7(float C4, float C5, float H3, float I2)
        {
            // =PI()*(C4+H3)*0.001*C5*0.001*H3*I2
            float I7 = (float)(Math.PI * (C4 + H3) * 0.001 * C5 * 0.001 * H3 * I2); ;
            return I7;
        }

        public static float calculateH10(float E16)
        {
            // =E16
            float H10 = E16;
            return H10;
        }

        public static float calculateH12(float C3, float H2, float H9, float H11)
        {
            // =PI()*(C3+H9)*0.001*H11*0.001*H9*H2
            float H12 = (float)(Math.PI * (C3 + H9) * 0.001 * H11 * 0.001 * H9 * H2);
            return H12;
        }

        public static float calculateI12(float C3, float H9, float H11, float I2)
        {
            // =PI()*(C3+H9)*0.001*H11*0.001*H9*I2
            float I12 = (float)(Math.PI * (C3 + H9) * 0.001 * H11 * 0.001 * H9 * I2);
            return I12;
        }

        public static float calculateH15(float C6, float C8, float C11, float H2, float H14)
        {
            // =((PI()*(H14/2*0.001)^2)-(PI()*(C6/2*0.001)^2)*C8)*C11*H2
            float H15 = (float)(Math.PI * Math.Pow((H14 / 2 * 0.001), 2) - (Math.PI * Math.Pow((C6 / 2 * 0.001), 2) * C8) * C11 * H2);
            return H15;
        }

        public static float calculateI15(float C6, float C8, float C11, float H14, float I2)
        {
            // =((PI()*(H14/2*0.001)^2)-(PI()*(C6/2*0.001)^2)*C8)*C11*I2
            float I15 = (float)(Math.PI * Math.Pow((H14 / 2 * 0.001), 2) - (Math.PI * Math.Pow((C6 / 2 * 0.001), 2) * C8) * C11 * I2);
            return I15;
        }

        public static float calculateH17(float C6)
        {
            float H17 = C6;
            return H17;
        }

        public static float calculateI17(float C7)
        {
            float I17 = C7;
            return I17;
        }

        public static float calculateH18(float C6, float C7, float C9, float H2)
        {
            // =(C6-C7)*C7*PI()*H2*0.001*C9*0.001
            float H18 = (float)((C6 - C7) * C7 * Math.PI * H2 * 0.001 * C9 * 0.001);
            return H18;
        }

        public static float calculateI18(float C6, float C7, float C9, float I2)
        {
            // =(C6-C7)*C7*PI()*H2*0.001*C9*0.001
            float I18 = (float)((C6 - C7) * C7 * Math.PI * I2 * 0.001 * C9 * 0.001);
            return I18;
        }

        /**
         * @H18 is calculated in previous method
         */
        public static float calculateH19(float H18, float C8)
        {
            // =H18*C8
            float H19 = H18 * C8;
            return H19;
        }

        /**
         * @I18 is calculated in previous method
         */
        public static float calculateI19(float I18, float C8)
        {
            // =I18*C8
            float I19 = I18 * C8;
            return I19;
        }

        public static float calculateH20(float C6, float C7, float H2)
        {
            // =(C6-C7)*C7*PI()*H2*0.001
            float H20 = (float)((C6 - C7) * C7 * Math.PI * H2 * 0.001);
            return H20;
        }

        public static float calculateI20(float C6, float C7, float I2)
        {
            // =(C6-C7)*C7*PI()*I2*0.001
            float I20 = (float)((C6 - C7) * C7 * Math.PI * I2 * 0.001);
            return I20;
        }

        public static float calculateH26(float C3, float H24)
        {
            // =C3-H24
            float H26 = C3 - H24;
            return H26;
        }

        public static float calculateH27(float C6, float C8, float H2, float H23, float H25, float H26)
        {
            // =(PI()*(H26/2*0.001)^2*(1-H25*0.01)-PI()*(C6/2*0.001)^2*C8)*H23*H2
            float H27 = (float)(Math.PI * Math.Pow((H26/2*0.001), 2) * (1 - H25 * 0.01) - Math.PI * Math.Pow((C6/2*0.001),2) * C8) * H23 * H2;
            return H27;
        }

        public static float calculateI27(float C6, float C8, float H23, float H25, float H26, float I2)
        {
            // =(PI()*(H26/2*0.001)^2*(1-H25*0.01)-PI()*(C6/2*0.001)^2*C8)*H23*H2
            float I27 = (float)(Math.PI * Math.Pow((H26 / 2 * 0.001), 2) * (1 - H25 * 0.01) - Math.PI * Math.Pow((C6 / 2 * 0.001), 2) * C8) * H23 * I2;
            return I27;
        }

        /**
         * @H27 is calculated in previous method
         */
        public static float calculateH28(float H22, float H27)
        {
            float H28 = H27 * H22;
            return H28;
        }

        /**
         * @I27 is calculated in previous method
         */
        public static float calculateI28(float H22, float I27)
        {
            float I28 = I27 * H22;
            return I28;
        }
    }
}
